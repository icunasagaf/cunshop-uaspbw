list_order = [];

function tampil(){
    var tabel = document.getElementById('tabel');
    tabel.innerHTML = "<tr><th>#</th><th>Order List</th><th>Quantity</th><th>Action</th></tr>"

    for (i=0; i<list_order.length; i++){
        var ganti = "<button class='btn btn-light fw-bold mt-0 btn-sm' href='#ol' onclick='edit(" + i + ")'>Edit</button>";
        var hapus = "<button class='btn btn-light fw-bold mt-0 btn-sm' href='#ol' onclick='hapus(" + i + ")'>Hapus</button>";
        j = i + 1;
        tabel.innerHTML += "<tr><th>" + j + "</th><td>" + list_order[i].menu_pesanan + "</td><td>" + list_order[i].jumlah_pesanan + "</td><td>" + ganti + " " + hapus + "</td></tr>"
    }
}

function tambah(){

    var order=[];
    var menu_pesanan = document.getElementById('menu_pesanan').value;
    var jumlah_pesanan = document.getElementById('jumlah_pesanan').value;

    order['menu_pesanan']= menu_pesanan;
    order['jumlah_pesanan']= jumlah_pesanan;
    list_order.push(order);

    var input1 = document.querySelector("input[name=menu_pesanan");
    var input2 = document.querySelector("input[name=jumlah_pesanan");
    
    tampil();

    input1.value = "";
    input2.value = "";
}

function edit(id) {
    var baru = prompt("Edit", list_order[id].menu_pesanan);
    var baru2 = prompt("Edit", list_order[id].jumlah_pesanan);

    list_order[id].menu_pesanan = baru;
    list_order[id].jumlah_pesanan = baru2;

    tampil();
}

function hapus(id){
    list_order.splice(id,1);
    tampil();
}






